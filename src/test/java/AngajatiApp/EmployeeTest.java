package AngajatiApp;

import AngajatiApp.model.Employee;
import org.junit.*;

import static org.junit.Assert.*;

public class EmployeeTest {

    private Employee e1, e11, e12;
    private Employee e2, e21;
    private Employee e3, e31;
    private Employee e4, e41;

    //se executa automat inaintea fiecarei metode de test
    @Before
    public void setUp() {
        e1 = new Employee();
        e1.setFirstName("Ionut");
        e12 = new Employee();
        e12.setFirstName("Ionut");
        e11 = new Employee();
        e11.setCnp("1234567890123");
        e2 = new Employee();
        e2.setLastName("Popescu");
        e3 = new Employee();
        e4 = null;
        e41 = null;
        System.out.println("Before test");
    }

    //se executa automat dupa fiecarei metoda de test
    @After
    public void tearDown() {
        e1 = null;
        e2 = null;
        e3 = null;

        System.out.println("After test");
    }

    /////**************
    @Test
    public void testGetFirst() {
        assertEquals("Ionut", e1.getFirstName());
    }

    @Test
    public void testSetFirst() {
         e12.setFirstName("jjjj");
    }

    @Test
    public void testGetterSetterLastName() {
        final String postalCode = "456098";
        e41 = new Employee();
        e41.setLastName("Popescullll");
    }
        @Test
    public void testGetLastName() {
        assertEquals("Popescu", e2.getLastName());
    }
    @Test
    public void testGetCNP(){
        assertEquals("1234567890123", e11.getCnp());
    }

    @Test
    public void testGetLastName2() {
        assertEquals("Popescu", e2.getLastName());
    }

    @Test(expected = NullPointerException.class)
    public void testFirstName() {
        assertEquals("Popescu", e4.getFirstName());
    }

    @Test(expected = NullPointerException.class)
    public void testLastName() {
        assertEquals("Popeslcu", e4.getLastName());
    }

    @Test(timeout = 100) //asteapta 10 milisecunde
    public void testFictiv() {
        try {
            Thread.sleep(101);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
        @Test
        public void testConstructor () {
            assertNotEquals("verificam daca s-a inserat angagatul", e1, null);
        }

        @BeforeClass
        public static void setUpAll () {
            System.out.println("Before All tests - at the beginning of the Test Class");
        }
        @AfterClass
        public static void tearDownAll () {
            System.out.println("After All tests - at the end of the Test Class");
        }
    }
