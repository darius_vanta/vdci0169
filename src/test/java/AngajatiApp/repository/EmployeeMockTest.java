package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeMockTest {
    private EmployeeMock repositoryEmployee;
    @Before
    public void setUp() {
        repositoryEmployee = new EmployeeMock();
    }

    @After
    public void tearDown() {
        repositoryEmployee = null;
        System.out.println("After test");
    }
    @Test
    public void modifyEmployeeFunctionTC02() {
        Employee Marius = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        String  listEmpl = repositoryEmployee.getEmployeeList().toString();
        repositoryEmployee.modifyEmployeeFunction(Marius, DidacticFunction.LECTURER);
        //System.out.println(listEmpl.toString());
        assertNotEquals(listEmpl, repositoryEmployee.getEmployeeList().toString());
         }

    @Test
    public void modifyEmployeeFunctionTC01() {

        String listEmpl = repositoryEmployee.getEmployeeList().toString();
        repositoryEmployee.modifyEmployeeFunction(null, DidacticFunction.ASISTENT);
        assertEquals(listEmpl, repositoryEmployee.getEmployeeList().toString());
    }

    @Test
    public void TC1(){
        Employee testEmployee = new Employee("Yo", "Da","1234567890123",DidacticFunction.ASISTENT, 2100.0);
        boolean result1 = repositoryEmployee.addEmployee(testEmployee);
        assertTrue(result1);
    }
    @Test
    public void TC2(){
        Employee testEmployee = new Employee("Yo", "Da","123",DidacticFunction.ASISTENT, 2100.0);
        boolean result2 = repositoryEmployee.addEmployee(testEmployee);
        assertFalse(result2);
    }
    @Test
    public void TC3(){
        Employee testEmployee = new Employee("Yo", "Da","1234567890123",DidacticFunction.ASISTENT, 300.0);
        boolean result3 = repositoryEmployee.addEmployee(testEmployee);
        assertFalse(result3);
    }
    @Test
    public void TC4() {
        Employee testEmployee = new Employee("Y", "Da", "1234567890123", DidacticFunction.ASISTENT, 2100.0);
        boolean result4 = repositoryEmployee.addEmployee(testEmployee);
        assertFalse(result4);
    }
        @Test
        public void TC5(){
            Employee testEmployee = new Employee("Yo", "D","1234567890123",DidacticFunction.ASISTENT, 2100.0);
            boolean result5 = repositoryEmployee.addEmployee(testEmployee);
            assertFalse(result5);
    }
        @Test
        public void TC6(){
            Employee testEmployee = new Employee("Yo", "Da","12345678901231",DidacticFunction.ASISTENT, 2100.0);
            boolean result6 = repositoryEmployee.addEmployee(testEmployee);
            assertFalse(result6);
        }


}